# Sterling *VMware vRealize Workshop* Lab Guide

<img src="images/Sterling-Logo-black.png" width="400"/><br>
<img src="images/VMW_09Q3_LOGO_Corp_Gray-01.png" width="400"/>

## Useful vRealize Suite Learning Tools
* <a href="https://learncodestream.github.io/">Learn CodeStream</a><br>
* <a href="https://learncloudassembly.github.io/">Learn Cloud Accembly</a><br>
* <a href="https://learnservicebroker.github.io/">Learn Service Broker</a><br>
* <a href="https://learnsaltstackconfig.github.io/">Learn SaltStack Config</a><br>
* <a href="https://learnorchestrator.github.io/">Learn Orchestrator</a><br>

## Lab Setup


1. Access your Horizon environment using the instructions and account information provided by during the workshop

2. Open Powershell to clone this repository for access to required resources

3. Clone the repository to your virtual desktop

        git clone https://gitlab.com/Drudgus/vrealize-labs.git

*This will hold the Templates in your local machine in case you need to refer to them later*

## Log Into vRealize Automation

1. From a browser log into https://vra-lab.sterling.lab

2. Click on *"GO TO LOGIN PAGE"*

3. Use the Sterling.Lab domain on the login page, and login with your username and password provided. (Username is not UPN format, just the SN).

4. Click on Cloud Assembly
<img src="images/Cloud-assembly.png" width="400"/>

5. Click on the *"Design"* tab

## Create a New Cloud Template

1. On Cloud Templates click on *"New From"* and select *"Blank Canvas"*

2. *"Name"* should be set to your lab Username, and the project will autopopulate when you click on the field. Then click *Create*

*From here you should be able to see the three sections of the Cloud Template, On the left is the resources that can be dragged into the canvas(the middle) and the right side will show the code.

3. Drag *vSphere Machine* over to the canvas, and then drag *vSphere Network* to the canvas, once there, highlight the circle on the network and connect it to the machine. *This is used for multiple functions on the canvas. Dragging resources, and connecting them via the circle can do multiple things defined by your use case.*

<img src="images/vSphere-Canvas.gif" width="600"/>

4. Remove the CPU and Memory lines from the Code, and add the following lines.

        flavor: ""

5. Insert your cursor in-between the "image:" quotes. You should see a "intellisense" pop up with the list of templates available to the project(Pick ubuntu). Do this for "flavor:" as well.(pick small) The code should look like this now.

        image: "ubuntu"
        flavor: "small"

*At this point you are deploying a base image that will connect to a network and pull a DHCP address. However, nothing is being installed on the machine from boot. Lets use Cloud-Init to deploy an application and start with something small like apache.*

## Using Cloud-Init

Cloud-init is a bootstrapping solution that installs software or runs updates at the point the machine is built. This is extremely useful as it expands your capabilities for automation when allowing users to self-serve new machines.

1. Add the following after the *"flavor"*:

              cloudConfig: |
                #cloud-config
                hostname: ${input.name}
                runcmd:
                  - sudo apt update
                  - sudo apt install nginx -y

*You will see a red exclamation mark showing an error, because we are using an input that we have not defined. So lets define that.*

2. Click on *"Inputs"* then *"New Cloud Template Input"* and set *"Name"* to *"name"*

<img src="images/inputs.gif" width="600"/>

If the syntax is showing errors the indetation may be incorrect, and if so look <a href="https://gitlab.com/Drudgus/vrealize-labs/-/blob/main/Ref/nginx.yaml">here</a> to copy the whole blueprint over.

2. With the blueprint complete click the *"Test"* button on the bottom left. Insert your username as the VM name and click *"Test"* This should respond with a green response.

<img src="images/test-successful.png" width="400"/>

3. Deploy the machine with the username as the deployment name and Input for *"name"*. 

<img src="images/vSphere-deploy.gif" width="600"/>

4. Watch the deployment run. Did it finish? What is missing?

*The deployment didn't fail. It simply needs an approval. All users in the group can approve the requests.*

5. Click on the *"9-Dots"* in the upper right hand, and click on *"Service Broker"*

<img src="images/service-broker1.png" width="400"/>

6. Click on *"Approvals"* tab

<img src="images/service-broker5.png" width="400"/>

7. Select the deployment you inputted, and then on *"Actions"* select *"Approve"*

8. This will send you to the approval response window. No description is needed, simply input *"Approve"*

<img src="images/service-broker6.png" width="400"/>

9. You can go back to *"Resources"* and watch the deployment finish.

10. Copy the ip of the machine and paste it into the browser.

<img src="images/nginx.png" width="600"/>

*You have successfully deployed a VM with a template and t-shirt size, as well as network configuration and bootstrapping for application updates, upgrades, and installations.*

## SaltStack Config

1. With the already copied IP. Open a Terminal/Command Prompt/Powershell and SSH into the ubuntu box, with lab/VMware1!

        ssh lab@*IPADDRESS*

**IP address can be found here:**  
<img src="images/ip-address.png" width="400"/>

*Hit "yes" to permenantly add the SSH key to your list of known hosts.*

2. Elevate to administrator (Using same password)

        sudo su

3. Install the SaltStack agent and point it to the Salt-Master

        curl -o bootstrap-salt.sh -L https://bootstrap.saltproject.io
        sudo sh bootstrap-salt.sh -A 10.150.161.5

4. Open a new tab on your browser and login to the SaltStack Config server - https://salt-lab.sterling.lab Login with your lab username/password

5. Click on *"Minion Keys"* > *"Pending"*

*All minions must first have their key approved, this can be handled many different ways, but we wanted you to see the manual process to accept the key.(For more information go to: https://intothesaltmine.readthedocs.io/en/latest/chapters/command-and-control/key-management.html )

6.  Select your machine and then click *"Accept Key"* and *"Accept Key"* again.

<img src="images/accept-minion-key.gif" width="600"/>

7. Click on *"Minions"*

6. Click on *"Create Target"*
*Here you have the ability to target your automation manifest to a group of machines. We will just use your machine you just deployed*

Name = Username  
Key = host  
Value = "Servername"  

<img src="images/salt-target.gif" width="600"/>

7. Click on *"Config"* and select *"Jobs"*. Here you will be able to create a new job for your minion

8. On the upper right click *"Create Job"*. Name it the username you were provided. 

Name = *"username"*  
Targets = *"username"*  
Function = "state.sls"  
Environment = "sse"  
States = "apache"  

*click save*

<img src="images/salt-job.png" width="400"/>

9. Run job: Click on the three dots next to the name of your job, and select *"Run Now"* all the defaults for this should be taken.

*You can go into "Activity" and check out the job in progress to see what the outcome is by clicking on the JID (Scroll to the right to see JID).*

*The job failed(Look at the Failed number), because NGINX is using port 80. To get the job to complete, we'll need to remove NGINX.*

10. Back to Terminal/Command Prompt/Powershell on the machine run to remove nginx

        sudo apt purge nginx
        sudo systemctl stop nginx

11. Re-run Job to install apache

*You can watch the job again from the JID and this time it will complete.*

<img src="images/salt-job-complete.png" width="400"/>

12. Refresh nginx website to see it has changed to apache!

<img src="images/apache2.png" width="600"/>

## CodeStream Lab

*In this lab we will connect to a Tanzu enabled vSphere environment and deploy a cluster. This cluster will be configured via your manifest and deployed through CodeStream CI, utilizing an orchestrating kubernetes supervisor cluster and API.*

1. Go back to your tab with vRealize Automation and log into CodeStream on the 9 dot menu.

*If you get a *"Guided Setup"* screen, just hit *"Continue"*.

2. Select *"Pipelines"*

*Here we are going to create our own Tanzu cluster within an already prescribed supervisor namespace*

3. Select *"New Pipelines"* and select *"Blank Canvas"*

Projects = *"labs"*  
Name = *"username"*

4. Click on *"+Stage"* to add a new stage and name it *"Cluster Creation"* click on *"Sequential Task"* to add a new task:

<img src="images/pipeline-1.gif" width="600"/>

5. This task will create the login script to utilize on the namespace. 

**Task Name** = *Login*  
**Type** = *CI*  
**Steps:** 

    tee expect.sh > /dev/null << EOF
    #!/usr/bin/expect -f
    set timeout -1
    spawn kubectl vsphere login --server 10.49.0.2 --insecure-skip-tls-verify --vsphere-username ${var.username}
    match_max 100000
    expect -exact "\r
    Password: "
    send -- "${var.password}\r"
    expect eof
    EOF

    chmod 777 expect.sh

6. Add a new *"Sequential Task"* named *Create Cluster*

Task Name = *"Create Cluster Script"*  
Type = *"CI"*  
Steps:  

    ./expect.sh

    tee cluster.yaml > /dev/null << EOF
    apiVersion: run.tanzu.vmware.com/v1alpha1
    kind: TanzuKubernetesCluster
    metadata:
      name: ${input.name}
      namespace: vralabs
    spec:
      distribution:
        version: v1.18
      topology:
        controlPlane:
          class: best-effort-xsmall
          count: 1
          storageClass: tanzu-nfs
        workers:
          class: best-effort-xsmall
          count: 1
          storageClass: tanzu-nfs
      settings:
        network:
          cni:
            name: calico
        storage:
          defaultClass: tanzu-nfs
    EOF

    kubectl config use-context vralabs
    kubectl apply -f cluster.yaml

    
**If Steps auto-populates with the code from the previous task, remove it and add the above Steps**  

**Each task section is very whitespace sensitive(Make sure its in the right format) For a friendlier version of the tasks to copy paste pleasse check the "Ref" section in the project**

7. Add an input for name.

*Click on the Input tab on the top-left of the screen and click "Add"  
*Name = name  
*Value = "USERNAME"

<img src="images/pipeline-2.gif" width="600"/>

8. Now we need to setup the CI portion of the pipeline so it calls to a kubernetes cluster connects to a publick dockerhub registry and pulls our CI image.

*Click on the "Workspace" tab and select "kubernetes"*  
*Kubernetes API endpoint = "on-prem"*  
*Builder image URL = "vnerdynate/ci:1.0"*  
*Image registry = "docker-registry"*  
*Proxy type = "Load Balancer"*  

<img src="images/pipeline-3.gif" width="600"/>

9. Cick *"Save"* on bottom left and then under *"Actions"* click *"Enable"* to enable the pipeline, and then select *"Actions" > "Run"*  

*The deployment should take around 6-7 minutes **AFTER** you see the pipeline stating complete. The complete message means the pipeline has sent the data to the API, but the cluster takes time to boot up and configure*

10. Download the needed Tanzu CLI plugin for Kubectl and unzip the archive in PowerShell

        curl.exe -k https://tanzu.sterling.lab/wcp/plugin/windows-amd64/vsphere-plugin.zip -o vsphere-plugin.zip
        Expand-Archive .\vsphere-plugin.zip

11. Verify the version

        .\vsphere-plugin\bin\kubectl-vsphere.exe version

12. Now we will login to the cluster we just created. You will use your lab credentials to login to the cluster. Open a Terminal/Command Prompt/Powershell window and run:

        kubectl vsphere login --server 10.49.0.2 --insecure-skip-tls-verify --vsphere-username USERNAME@sterling.lab --tanzu-kubernetes-cluster-name USERNAME --tanzu-kubernetes-cluster-namespace vralabs

*You will be prompted for a password, here you will type in the password for the user. Now you will use the context of the new cluster to use that clusters resources*

        kubectl config use-context USERNAME

*Now you are in the cluster you have deployed. Lets see the nodes.*

        kubectl get nodes

*Congrats! You have successfully deployed a Tanzu Cluster from vRA as a CI pipeline.

## vRealize Service Broker and vRealize Orchestrator

*We're going to go through some policies and governance with Service Broker. Once you login you will see an Orchestrator solution already released to the catalog. First we'll show how approval policies work.

1. Click on the *"9-Dot"* menu on the top right and select *"Service Broker"*

<img src="images/service-broker1.png" width="400"/>

2. *The catalog should show a service for "Run SSH Command..." * Click Request.

<img src="images/service-broker2.png" width="400"/>

3. Fill out the custom form

<img src="images/service-broker3.png" width="400"/>

Deployment Name: *USERNAME-Orchestrator* ex. "ws-vra01-orchestrator"  
Host name or IP of the SSH host: "IP of your deployed VM" (In your SSH Terminal/Command Prompt/Powershell run *"ip a"* to find the IP)  

4. Click *"Submit"*

5. Watch the deployment run. Did it finish? What is missing?

<img src="images/service-broker4.png" width="400"/>

*The deployment didn't fail. Its your favorite approval workflow waiting to be remembered for what it does. Isn't governance cool?*

6. Click on *"Approvals"* tab

<img src="images/service-broker5.png" width="400"/>

7. Select the deployment you inputted, and then on *"Actions"* select *"Approve"*

8. This will send you to the approval response window. No description is needed, simply input *"Approve"*

<img src="images/service-broker6.png" width="400"/>

9. You can go back to *"Resources"* and watch the deployment finish.

10. Go back into your Terminal/Command Prompt/Powershell window which should still be connected to your box via SSH.

        ls

11. You should see the file *"working.txt"* when you run ls

<img src="images/service-broker7.png" width="400"/>

*Congrats! You have deployed something via vRA Orchestrator and Approved the workflow as an Approver. Think of all the ways this could be used for the solutions we used today. CodeStream, Cloud Assembly, Orchestrator all through Service Broker to create Self-Service solutions through your vRealize Automation Portal.*

## Cleanup

1. Click on Resources from either *"Cloud Assembly"* or *"Service Broker"* and delete all resources that show your username as the Owner.


